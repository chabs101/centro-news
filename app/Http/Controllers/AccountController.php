<?php

namespace App\Http\Controllers;

use App\table\userInformation;
use App\table\userLogs;
use App\table\userAccountType;
use Carbon\Carbon;
use Auth;
use App\User;
use Request;
use DB;
use Session;
use Hash;

class AccountController extends Controller
{
	public function AddUser() {
			try {
				DB::beginTransaction();

					$addaccount = new User();
					$addaccount->username 				= Request::get('username');
					$addaccount->password 				= bcrypt(Request::get('password'));
					$addaccount->account_type_id 		= Request::get('accounttype');
					$addaccount->save();

					$getaccid = User::where('username',Request::get('username'))->first();

					$adduserinfo = new userInformation();
					$adduserinfo->first_name 	= Request::get('fname');
					$adduserinfo->last_name 	= Request::get('lname');
					$adduserinfo->email 		= Request::get('email');
					$adduserinfo->gender 		= Request::get('gender');
					$adduserinfo->birthdate 	= Carbon::parse(Request::get('birthdate'));	
					$adduserinfo->user_id 		= $getaccid->user_id;
					$adduserinfo->save();


					$addlogs = new userLogs();
					$addlogs->task_do = Request::get('username').' has been created !';
					$addlogs->user_id = Auth::user()->user_id;
					$addlogs->save();

				DB::commit();
			}
			catch(\Illuminate\Database\QueryException $e) {
				DB::rollback();
				return $e->getMessage();
			}
			Session::flash('success',true);
			Session::flash('successmsg','W o W ! '. Request::get('username').' has been created !');
			return redirect()->back();
	}

	public function UpdateUserInformation() {
			$msg = new returnMessage();
			try {
				DB::beginTransaction();
					if(Request::has('position')) {
						$updaccount = User::find(Request::get('accountid'));
						$updaccount->account_type_id = Request::get('position');
						$updaccount->save();
					}

					$upduserinfo = userInformation::where('user_id',Request::get('accountid'))->first();
					$upduserinfo->first_name 	= Request::get('fname');
					$upduserinfo->last_name 	= Request::get('lname');
					$upduserinfo->email 		= Request::get('email');
					$upduserinfo->gender 		= Request::get('gender');
					$upduserinfo->birthdate 	= Carbon::parse(Request::get('birthdate'));	
					$upduserinfo->about 		= Request::has('about') ? Request::get('about') : 'none';
					$upduserinfo->save();


					$updlogs = new userLogs();
					$updlogs->task_do = Request::get('fname').' '.Request::get('lname').' has been updated !';
					$updlogs->user_id = Auth::user()->user_id;
					$updlogs->save();

				DB::commit();
			}
			catch(\Illuminate\Database\QueryException $e) {
				DB::rollback();
				return $e->getMessage();
			}
			return $msg->status(true)
						->message(Request::get('fname').' '.Request::get('lname').' has been updated !')
						->show();
	}

	public function UpdateUserAvatar() {
		$msg = new returnMessage();
		try {
			DB::beginTransaction();
			$imguser = Request::get('accountid').'.'.Request::file('userimg')->getClientOriginalExtension();
			Request::file('userimg')->move('assets/img/account',$imguser);
			$upduser = User::find(Request::get('accountid'));
			$upduser->picture = $imguser;
			$upduser->save();

			$updlogs = new userLogs();
			$updlogs->task_do	 = Request::get('accountid').' Change Avatar';
			$updlogs->user_id 	 = Auth::user()->user_id;
			$updlogs->save();

			DB::commit();
		}
		catch(\Illuminate\Database\QueryException $e) {
			return $e->getMessage();
		}
			return $msg->status(true)
						->message('W o w ! '.Request::get('accountid').' Avatar is successfully updated !')
						->show();
	}

	public function UpdateUserPassword() {
			$msg = new returnMessage();
				if(Request::has('password') && Request::has('newpassword')) {
					if(Request::get('password')!=Request::get('newpassword')) {
						return $msg->status(false)
									->message('Confirm password not equal')
									->show();
					}
				}

				if(Request::has('curpassword')) {
					if(Hash::check(Request::get('curpassword'),Auth::user()->password)==false ) {
						return $msg->status(false)
									->message('current password not equal')
									->show();	
					}
				}

			try {
				DB::beginTransaction();
					$updpass = User::find(Request::get('accountid'));
					$updpass->password = bcrypt(Request::get('newpassword'));
					$updpass->save();

					$updlogs = new userLogs();
					$updlogs->task_do = 'id '.Request::get('accountid').' Change password';
					$updlogs->user_id = Auth::user()->user_id;
					$updlogs->save();
					
				DB::commit();
			}
			catch(\Illuminate\Database\QueryException $e) {
					DB::rollback();
					return $e->getMessage();			
			}
				return $msg->status(true)
							->message('W o w ! '.Request::get('accountid').' Password is successfully updated !')
							->show();

	}

	public function UpdateUsername() {
			$msg = new returnMessage();
			try {
				DB::beginTransaction();
					$updusername = User::find(Request::get('accountid'));
					$updusername->username = Request::get('username');
					$updusername->save();
				DB::commit();
			}
			catch(\Illuminate\Database\QueryException $e) {
				DB::rollback();
				return $e->getMessage();
			}
				return $msg->status(true)
							->message('W o w ! '.Request::get('username').' Username is successfully updated !')
							->show();

	}

	public function changeStatus() {
		try {
			$upduser = User::find(Request::get('id'));
			$upduser->status = Request::get('status')=='Activate' ? null : 'Deactivate';
			$upduser->save();

			$upduserlog = new userLogs();
			$upduserlog->task_do 	= Request::get('username').' is '.Request::get('status'); 
			$upduserlog->user_id 	= Auth::user()->user_id;
			$upduserlog->save();
		}
		catch(\Illuminate\Database\QueryException $e) {
			DB::rollback();
			return $e->getMessage();
		}
			Session::flash('success',true);
			Session::flash('successmsg', Request::get('username').' has been '.Request::get('status').' !');
			return redirect()->back();
	}

}
