<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Session;
use Request;

class AuthController extends Controller
{

	public function view_login() {
		if(Auth::guest()) {
			return view('login');
		}else{
			return redirect('admin/dashboard');
		}
	}

	public function authUser() {

		if(Auth::attempt(['username' => Request::get('username'), 'password' => Request::get('password'),'status' => null]) ) {
			return redirect()->intended('admin/login');
		}
		else {
			Session::flash('fail',true);
			Session::flash('failmsg','Invalid Credentials !');
			return redirect('admin/login');
		} 
	}


	public function authLogout() {
		Session::flush();
		return redirect()->intended('admin/login');
	}
}
