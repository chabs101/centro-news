<?php

namespace App\Http\Controllers\Auth;
namespace App\Http\Controllers;

use Socialite;
use Mail;

class SocialiteController extends Controller
{
	public function redirectToProvider() {
		return Socialite::driver('github')->redirect();
	}

	public function handleProviderCallback() {
		$user = Socialite::driver('github')->user();
	}

	public function sendMail() {
        $verificationCode = str_random(20);
        try {
	        Mail::send('mail.sendmail', ['vcode' => $verificationCode], function($message)
	        {
	            $message->to('raneil.chavez@gmail.com', 'aneligop')->subject('RJC!');
	        });
	    }
	    catch(MailException $e) {
	    	echo $e->getMessage();
	    }
	}

}
