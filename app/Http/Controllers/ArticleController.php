<?php

namespace App\Http\Controllers;

use App\table\Article;
use App\table\ArticleLog;
use App\table\Category;
use App\table\subCategory;
use App\table\province;
use App\table\newsImages;
use Request;
use DB;
use Auth;
use Session;

class ArticleController extends Controller
{
    public function addarticle() {

        $checkarticle = Article::where('headline',Request::get('headline'))->where('subheading',Request::get('subheading'))->count();

        if($checkarticle>0) {
            Session::flash('fail',true);
            Session::flash('failmsg','O o o p s ! '.Request::get('headline').' is already exist !');
            return redirect()->back();
        }

        try {
            DB::beginTransaction();
                $addarticle = new Article();
                $addarticle->headline       = Request::get('headline');
                $addarticle->subheading     = Request::get('subheading');
                $addarticle->body           = Request::get('articlebody');
                $addarticle->status         = Request::has('status') ? Request::get('status') : "Pending";
                $addarticle->province_id    = Request::get('province');
                $addarticle->category_id    = Request::get('category');
                $addarticle->user_id        = Auth::user()->user_id;
                $addarticle->save();

                $getid = Article::where("headline",Request::get("headline"))->first();

                $articleimg = $getid->article_id.'.'. (Request::has('articleimg') ? Request::file('articleimg')->getClientOriginalExtension() : '');
                Request::has('articleimg') ? Request::file('articleimg')->move('assets/img/news',$articleimg) : '';
                $addarticleimg = new newsImages();
                $addarticleimg->image       = $articleimg;
                $addarticleimg->article_id  = $getid->article_id;
                $addarticleimg->save();

                $addarticlelog = new ArticleLog();
                $addarticlelog->user_id     = Auth::user()->user_id;
                $addarticlelog->task_do     = "Add Article ". Request::get('headline');
                $addarticlelog->article_id  = $getid->article_id;
                $addarticlelog->save();

           DB::commit();
        }
        catch(Illuminate\Database\QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        }

        Session::flash('success',true);
        Session::flash('successmsg','W o w ! '.Request::get('headline').' is successfully inserted !');
        return redirect()->back();  
    }

    public function updatearticle() {
        try {
            DB::beginTransaction();
                $updarticle = Article::find(Request::get('articleid'));
                $updarticle->headline       = Request::get('headline');
                $updarticle->subheading     = Request::get('subheading');
                $updarticle->body           = Request::get('articlebody');
                $addarticle->province_id    = Request::get('province');
                $addarticle->category_id    = Request::get('category');
                $updarticle->save();

                if(Request::has('articleimg')) {
                    $articleimg = Request::get('articleid').'.'. (Request::has('articleimg') ? Request::file('articleimg')->getClientOriginalExtension() : '');
                    Request::has('articleimg') ? Request::file('articleimg')->move('assets/img/news',$articleimg) : '';
                    $updatearticleimg = newsImages::find(Request::get('articleid'));
                    $updatearticleimg->image       = $articleimg;
                    $updatearticleimg->save();
                }

                $updarticlelog = new ArticleLog();
                $updarticlelog->user_id     = Auth::user()->user_id;
                $updarticlelog->task_do     = "Update Article ". Request::get('headline');
                $updarticlelog->article_id  = Request::get('articleid');
                $updarticlelog->save();

           DB::commit();
        }
        catch(Illuminate\Database\QueryException $e) {
            DB::rollback();
            return $e->getMessage();
        }

        Session::flash('success',true);
        Session::flash('successmsg','W o w ! '.Request::get('headline').' is successfully updated !');
        return redirect()->back();  
    }

}
