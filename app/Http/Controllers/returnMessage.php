<?php
namespace App\Http\Controllers;

class returnMessage {
	protected $status;
	protected $msg;
	
	public function status($status) {
		$this->status = $status;
		return $this;
	}
	public function message($message) {
		$this->msg = $message;
		return $this;
	}

	public function show() {
		$holder = array();
		$holder[0] = $this->status;
		$holder[1] = $this->msg;
		return $holder;
	}
}

?>