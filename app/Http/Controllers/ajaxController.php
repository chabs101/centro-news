<?php

namespace App\Http\Controllers;

use App\table\Article;
use App\table\ArticleLog;
use App\table\userInformation;
use App\table\province;
use App\table\category;
use Carbon\Carbon;
use Request;
use Auth;
use Hash;
use DB;
class ajaxController extends Controller
{

	public function checklogin(){
			$passinput = Request::get('old_pass');
			$oldpass = Auth::where('username',Request::get('username'))->password;

			if(Hash::check($passinput, $oldpass)){
				echo "success";
			}else{
				echo "fail";
			}		
	}

	public function confirmarticle() {
		try {
			if(Request::has('approved')) {
				ajaxController::ApprovedArticle(Request::get('articleid'),Request::get('headline'));
			}
			elseif(Request::has('rejected')) {
				ajaxController::RejectArticle(Request::get('articleid'),Request::get('headline'));
			}
		}
		catch(\Illuminate\Database\QueryException $e) {
			return $e->getMessage();
		}
	}

	public function getAccount() {
		// $useraccount = DB::table('getAccount')->groupBy('user_id')->get();
		$useraccount 	= userInformation::with('getAccount.getType','getAccountLogs')->where('user_id','!=','1')->get();
		echo json_encode($useraccount);
	}

	public function getBody() {
		$getbody = Article::find(Request::get('id'));
		return $getbody->body;
	}

	public function checkUsername() {
		
	}

	public function getprovince() {
		$prov = DB::table('province')->orderBy('province','ASC')->get();
		echo json_encode($prov);
	}

	public function getcategory() {
		$cat = DB::table('category')->orderBy('category','ASC')->get();
		echo json_encode($cat);
	}

	public function addprovince() {
		$msg = new returnMessage();
		$check = province::where('province',Request::get('province'))->first();
		if(count($check)>0) {
			return $msg->status(false)
						->message('O o o p s, Already added')
						->show();	
		}
		try {
			DB::beginTransaction();
			$addprovince = new province();
			$addprovince->province 	= Request::get('province');
			$addprovince->save();
			DB::commit();
		}
		catch(\Illuminate\Database\QueryException $e) {
			DB::rollback();	
			return $e->getMessage();
		}
		return $msg->status(true)
					->message('W o W, Add Province Success')
					->show();
	}

	public function addcategory() {
		$msg = new returnMessage();
		$check = category::where('category',Request::get('category'))->first();
		if(count($check)>0) {
			return $msg->status(false)
						->message('O o o p s, Already added')
						->show();
		}
		try {
			DB::beginTransaction();
			$addcategory = new category();
			$addcategory->category	= Request::get('category');
			$addcategory->save();
			DB::commit();
		}
		catch(\Illuminate\Database\QueryException $e) {
			DB::rollback();	
			return $e->getMessage();
		}
		return $msg->status(true)
					->message('W o W, Add Category Success')
					->show();
	}

	public function getnotification() {
		$notif = Article::with('getArticleLog')->where('status','Pending')->orderBy('created_at','desc')->get();
		return json_encode($notif->map(function($respo){
			return [
			'headline'	 => $respo->headline,
			'created_at' => $respo->created_at->diffForHumans(),
			];
		}));
	}

	public function ApprovedArticle($id,$headline) {
			$msg = new returnMessage();
			try {
				DB::beginTransaction();
					$confirmarticle = Article::find($id);
					$confirmarticle->status = "Approved";
					$confirmarticle->save();

					$confirmlog = new ArticleLog();
					$confirmlog->user_id 	= Auth::user()->user_id;
					$confirmlog->task_do 	= "Approved Article ". $headline;
					$confirmlog->article_id = $id;
					$confirmlog->save(); 
				DB::commit();
			}
			catch(\Illuminate\Database\QueryException $e) {
				DB::rollback();
				return $e->getMessage();
			}
			echo json_encode($msg->status(true)
						->message('W o w, Successfully approved !')
						->show());
	}

	public function RejectArticle($id,$headline) {
			$msg = new returnMessage();
			try {
				DB::beginTransaction();
					$confirmarticle = Article::find($id);
					$confirmarticle->status = "Rejected";
					$confirmarticle->save();

					$confirmlog = new ArticleLog();
					$confirmlog->user_id 	= Auth::user()->user_id;
					$confirmlog->task_do 	= "Rejected Article ". $headline;
					$confirmlog->article_id = $id;
					$confirmlog->save(); 
				DB::commit();
			}
			catch(\Illuminate\Database\QueryException $e) {
				DB::rollback();
				return $e->getMessage();
			}
			echo json_encode($msg->status(true)
						->message($headline.' is rejected !')
						->show());
	}

	public function testsearch() {
		$articlesearch = Article::where('headline','like','%'.Request::get('search').'%')->get();
		return json_encode($articlesearch);
	}

}
