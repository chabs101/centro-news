<?php

namespace App\table;

use Illuminate\Database\Eloquent\Model;

class newsImages extends Model
{
	protected $table = 'newsimages';
	protected $primaryKey = 'newsimage_id';
}
