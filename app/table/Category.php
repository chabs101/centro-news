<?php

namespace App\table;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table 		= 'category';
	protected $primaryKey 	= 'category_id';
	public $timestamps		= false;
}
