<?php

namespace App\table;

use Illuminate\Database\Eloquent\Model;

class userLogs extends Model
{
	protected $table = "tbl_userLogs";
	protected $primaryKey = "userlogs_id";

}
