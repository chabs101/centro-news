<?php

namespace App\table;

use Illuminate\Database\Eloquent\Model;

class userInformation extends Model
{
	protected $table = "tbl_userInformation";
	protected $primaryKey = "userinfo_id";
	public $timestamps = false;

	public function getAccount() {
		return $this->belongsTo('App\User','user_id','user_id');
	}

	public function getAccountLogs() {
		return $this->belongsTo('App\table\userLogs','user_id','user_id');
	}

}
