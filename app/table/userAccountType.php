<?php

namespace App\table;

use Illuminate\Database\Eloquent\Model;

class userAccountType extends Model
{

	protected $table 		= 'tbl_userAccount_type';
	protected $primaryKey 	= 'account_type_id';

}
