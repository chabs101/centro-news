<?php

namespace App\table;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table 		= 'article';
	protected $primaryKey 	= 'article_id';

	public function getArticleLog() {
		return $this->belongsTo('App\table\articleLog','article_id','article_id');
	}

	public function getArticleReporter() {
		return $this->belongsTo('App\table\userInformation','user_id','user_id');
	}

	public function getCategory() {
		return $this->belongsTo('App\table\category','category_id','category_id');
	}

	public function getProvince() {
		return $this->belongsTo('App\table\province','province_id','province_id');
	}
}
