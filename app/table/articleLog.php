<?php

namespace App\table;

use Illuminate\Database\Eloquent\Model;

class articleLog extends Model
{
	protected $table = 'tbl_articlelog';
	protected $primaryKey = 'articlelog_id';

	public function getArticleLog() {
		return $this->belongsTo('App\User','user_id','user_id');
	}

}
