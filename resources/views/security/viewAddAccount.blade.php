@extends('layout.layoutmain')

@section('title')
<title>Add Staff | CentroBalita</title>
@stop

@section('headscript')
        <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
@stop

@section('account')
active open
@stop
@section('accountadd') active open @stop

@section('content')

                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Dashboard
                        <small>dashboard & statistics</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard">Home</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS 1-->
                    <div class="row">
                                        <div class="portlet light bordered padding-left-10">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-equalizer font-red-sunglo"></i>
                                                    <span class="caption-subject font-red-sunglo bold uppercase">Add News</span>
                                                    <span class="caption-helper">form actions without bg color</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="#" class="form-horizontal" id="form_requirements">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">First name</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control" name="fname" placeholder="firstname" required>
                                                                <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">Last name</label>
                                                            <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="lname" placeholder="lastname" required> 
                                                                    <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">Email</label>
                                                            <div class="col-md-4">
                                                                    <input type="email" class="form-control" name="email" placeholder="email" required> 
                                                                    <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">Gender</label>
                                                            <div class="col-md-4">
                                                                <select name="gender" class="form-control" required>  
                                                                    <option value=""></option>
                                                                    <option value="Male">Male</option>
                                                                    <option value="Female">Female</option>
                                                                </select>
                                                                <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">Birthdate</label>
                                                            <div class="col-md-4">
                                                                    <input type="text" class="form-control date-picker" name="birthdate" placeholder="Birthdate" required> 
                                                                    <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">Account Type</label>
                                                            <div class="col-md-4">
                                                                <select name="accounttype" class="form-control" required>  
                                                                    <option value=""></option>
                                                                    <option value="Administrator">Administrator</option>
                                                                    <option value="Moderator">Moderator</option>
                                                                    <option value="Writer">Writer</option>
                                                                </select>
                                                                    <div class="form-control-focus"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">Username</label>
                                                            <div class="col-md-4">
                                                                <div class="input-icon input-icon-sm right">
                                                                    <i class="fa fa-user"></i>
                                                                    <input type="text" class="form-control input-sm" name="username" placeholder="Username"> 
                                                                    <div class="form-control-focus"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label">Password</label>
                                                            <div class="col-md-4">
                                                                <div class="input-icon input-icon-sm right">
                                                                    <i class="fa fa-key"></i>
                                                                    <input type="password" class="form-control input-sm" name="password" placeholder="password"> 
                                                                    <div class="form-control-focus"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-2 col-md-9">
                                                                <button type="submit" class="btn green">Submit</button>
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                    </div>
                    <div class="clearfix"></div>

@stop

@section('pagescript')

        <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/formvalidation.js" type="text/javascript"></script>
@stop
