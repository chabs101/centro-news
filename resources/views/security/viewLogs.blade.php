@extends('layout.layoutmain')

@section('title')
<title>Logs | CentroBalita</title>
@stop

@section('headscript')
        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@stop

@section('setting') active open @stop
@section('viewlog') active open @stop

@section('content')

                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Dashboard
                        <small>dashboard & statistics</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard">Home</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS 1-->
                    <div class="row">
                        
                    </div>
                    <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                    <div class="row">

                        <div class="col-md-12">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-share font-blue"></i>
                                        <span class="caption-subject font-blue bold uppercase">Recent Activities</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2" id="container">
                                            <thead>
                                                <tr>
                                                    <th data-sort-ignore="true">
                                                        <input type="checkbox" id="select-all">
                                                    </th>
                                                    <th> User</th>
                                                    <th> Activity</th>
                                                    <th> Date Inserted </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($activities as $activitiesVal)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" class="selecteditem" name="log[]"/>
                                                    </td>
                                                    <td width="5%"><img src="../assets/img/account/{{$activitiesVal->picture}}" width="50px" height="50px" style="border:2px #fff;"></td>
                                                    <td>{{$activitiesVal->task_do}}</td>
                                                    <td>{{$cparser->parse($activitiesVal->created_at)}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                                        <button class="btn btn-danger">DELETE</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                
@stop

@section('pagescript')
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $('#select-all').click(function(event){
        if(this.checked) {
            $('.selecteditem').each(function() {
                this.checked = true;
               $('span').addClass('checked');
            });
        } 
        else {
            $('.selecteditem').each(function() {
                this.checked = false;
               $('span').removeClass('checked');
            });
        }
    });
</script>

@stop