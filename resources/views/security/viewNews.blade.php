@extends('layout.layoutmain')

@section('title')
<title>News List | CentroBalita</title>
@stop

@section('headscript')
        <link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
@stop

@section('addnewsPage')
active open
@stop

@section('content')
                    <h3 class="page-title"> Dashboard
                        <small>dashboard & statistics</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard">Home</a>
                            </li>
                        </ul>
                    </div>
                    @include('include.response')
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-notebook font-purple-soft"></i>
                                        <span class="caption-subject font-purple-soft bold uppercase">NEWS</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1_1" data-toggle="tab"> Pending </a>
                                        </li>
                                        <li>
                                            <a href="#tab_1_2" data-toggle="tab"> Approved </a>
                                        </li>
                                        <li>
                                            <a href="#tab_1_3" data-toggle="tab"> Rejected </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="tab_1_1">
                                                            
                                            <div class="portlet light ">
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="portlet_comments_1">
                                                            <!-- BEGIN: Comments -->
                                                            @foreach($articlepending as $articleVal)
                                                            <div class="mt-comments" id="articlepending{{$articleVal->article_id}}">
                                                                <div class="mt-comment">
                                                                    <div class="mt-comment-img">
                                                                        <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                                                                    <div class="mt-comment-body">
                                                                        <div class="mt-comment-info">
                                                                            <span class="mt-comment-author">{{$articleVal->getArticleReporter->first_name.' '.$articleVal->getArticleReporter->last_name}}</span>
                                                                            <span class="mt-comment-date">{{date('F m, Y',strtotime($articleVal->created_at))}}</span>
                                                                        </div>
                                                                                    <div class="mt-action-buttons pull-right" id="btnpending{{$articleVal->article_id}}">
                                                                                        <div class="btn-group btn-group-circle">
                                                                                            <button type="button" class="btn btn-outline green btn-sm Approved_confirmation" 
                                                                                                    data-toggle="confirmation" 
                                                                                                    data-article-id="{{$articleVal->article_id}}"
                                                                                                    data-headline="{{$articleVal->headline}}"
                                                                                                    data-articlepending-id="articlepending{{$articleVal->article_id}}" 
                                                                                                    data-btn-id="btnpending{{$articleVal->article_id}}"
                                                                                                    id="Approved_confirmation">Approve</button>
                                                                                            <button type="button" class="btn btn-outline red btn-sm Reject_confirmation" 
                                                                                                    data-toggle="confirmation" 
                                                                                                    data-article-id="{{$articleVal->article_id}}"
                                                                                                    data-headline="{{$articleVal->headline}}"
                                                                                                    data-articlepending-id="articlepending{{$articleVal->article_id}}" 
                                                                                                    data-btn-id="btnpending{{$articleVal->article_id}}"
                                                                                                    id="Reject_confirmation">Reject</button>
                                                                                        </div>
                                                                                    </div>
                                                                        <div class="mt-comment-text"><b>{{$articleVal->headline}}</b> <i>{{$articleVal->subheading}}</i></div>
                                                                        <br/>
                                                                        <div class="mt-comment-details">
                                                                            <span class="mt-comment-status mt-comment-status-pending font-yellow-crusta pending-article-{{$articleVal->article_id}}">{{$articleVal->status}}</span>
                                                                            <ul class="mt-comment-actions">
                                                                                <li>
                                                                                    <a href="#" class="viewArticle" data-toggle="modal" 
                                                                                        data-target="#viewEditModal" 
                                                                                        data-article-id="{{$articleVal->article_id}}" 
                                                                                        data-headline="{{$articleVal->headline}}" 
                                                                                        data-subheading="{{$articleVal->subheading}}"
                                                                                        >Quick Edit</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">View</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            @if(count($articlepending) == 0) 
                                                                <span class="form-control text-center bg-blue font-white">NO ARTICLE PENDING</span>
                                                            @endif
                                                            <!-- END: Comments -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane fade" id="tab_1_2">

                                                        <div class="tab-pane" id="approvedarticlecontainer">
                                                            <!-- BEGIN: Comments -->
                                                            @foreach($article->where('status','Approved') as $articleVal)
                                                            <div class="mt-comments" >
                                                                <div class="mt-comment">
                                                                    <div class="mt-comment-img">
                                                                        <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                                                                    <div class="mt-comment-body">
                                                                        <div class="mt-comment-info">
                                                                            <span class="mt-comment-author">{{$articleVal->getArticleReporter->first_name.' '.$articleVal->getArticleReporter->last_name}}</span>
                                                                            <span class="mt-comment-date">{{date('F m, Y',strtotime($articleVal->created_at))}}</span>
                                                                        </div>
                                                                        <div class="mt-comment-text"><b>{{$articleVal->headline}}</b> <i>{{$articleVal->subheading}}</i></div>
                                                                        <div class="mt-comment-details">
                                                                            <span class="mt-comment-status font-green">{{$articleVal->status}}</span>
                                                                            <ul class="mt-comment-actions">
                                                                                <li>
                                                                                    <a href="#" class="viewArticle" data-toggle="modal" 
                                                                                        data-target="#viewEditModal" 
                                                                                        data-article-id="{{$articleVal->article_id}}" 
                                                                                        data-headline="{{$articleVal->headline}}" 
                                                                                        data-subheading="{{$articleVal->subheading}}"
                                                                                        >Quick Edit</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">View</a>
                                                                                </li>
                                                                                <li class="{{Auth::user()->account_type_id==1 ? '' : 'hidden'}}">
                                                                                    <a href="#">Delete</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            @if(count($article->where('status','Approved'))==0)
                                                            <span class="form-control text-center bg-blue font-white">NO ARTICLE APPROVED</span>
                                                            @endif
                                                            {{$article->render()}}
                                                            <!-- END: Comments -->
                                                        </div>

                                        </div>

                                        <div class="tab-pane fade" id="tab_1_3">

                                                        <div class="tab-pane" id="rejectedarticlecontainer">
                                                            <!-- BEGIN: Comments -->
                                                            @foreach($article->where('status','Rejected') as $articleVal)
                                                            <div class="mt-comments" >
                                                                <div class="mt-comment">
                                                                    <div class="mt-comment-img">
                                                                        <img src="../assets/pages/media/users/avatar1.jpg" /> </div>
                                                                    <div class="mt-comment-body">
                                                                        <div class="mt-comment-info">
                                                                            <span class="mt-comment-author">{{$articleVal->getArticleReporter->first_name.' '.$articleVal->getArticleReporter->last_name}}</span>
                                                                            <span class="mt-comment-date">{{date('F m, Y',strtotime($articleVal->created_at))}}</span>
                                                                        </div>
                                                                        <div class="mt-comment-text"><b>{{$articleVal->headline}}</b> <i>{{$articleVal->subheading}}</i></div>
                                                                        <div class="mt-comment-details">
                                                                            <span class="mt-comment-status font-red">{{$articleVal->status}}</span>
                                                                            <ul class="mt-comment-actions">
                                                                                <li>
                                                                                    <a href="#" class="viewArticle" data-toggle="modal" 
                                                                                        data-target="#viewEditModal" 
                                                                                        data-article-id="{{$articleVal->article_id}}" 
                                                                                        data-headline="{{$articleVal->headline}}" 
                                                                                        data-subheading="{{$articleVal->subheading}}"
                                                                                        >Quick Edit</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">View</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="#">Delete</a>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            @if(count($article->where('status','Approved'))==0)
                                                            <span class="form-control text-center bg-blue font-white">NO ARTICLE REJECTED</span>
                                                            @endif
                                                            {{$article->render()}}
                                                            <!-- END: Comments -->
                                                        </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


@include('include.EditNewsModal')
@include('include.splash')

@stop

@section('pagescript')
        <script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/formvalidation.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/uiconfirmation.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
        <script src="../assets/apps/custom/ajaxSave.js" type="text/javascript"></script>
        <script type="text/javascript">
        var oldresp;
        $('.viewArticle').click(function() {
            $('.splash-body').show();
            $('input[name=articleid]').val($(this).attr('data-article-id'));
            $('input[name=headline]').val($(this).attr('data-headline'));
            $('input[name=subheading]').val($(this).attr('data-subheading'));
            $('#summernote_1').html("");
            $.get('/ajax/body',{id :$(this).attr('data-article-id')},function(response){
                if(response!=oldresp) {
                    $('#summernote_1').code(response);
                    $('.splash-body').fadeOut('slow');
                    oldresp = response;
                }
            });

        });
        </script>
@stop