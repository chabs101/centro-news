@extends('layout.layoutmain')

@section('title')
<title>Add News | CentroBalita</title>
@stop

@section('headscript')
        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
        <link href="../assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

@stop

@section('account')
active open
@stop
@section('accountview') active open @stop

@section('content')

                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Dashboard
                        <small>dashboard & statistics</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard">Home</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            @include('include.response')
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-green">
                                        <i class="icon-settings font-green"></i>
                                        <span class="caption-subject bold uppercase">Basic</span>
                                    </div>
                                    <div class="tools"> 
                                        <a href="#"  class="reload"></a>
                                    </div>
                                </div>
                                <br/><br/><br/>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="all">Name</th>
                                                <th class="min-tablet">Position</th>
                                                <th class="min-tablet">Gender</th>
                                                <th class="min-tablet">Date Created</th>
                                                <th class="desktop">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($useraccount as $useraccountVal)
                                                    <tr>
                                                        <th></th>
                                                        <td>{{$useraccountVal->first_name.' '.$useraccountVal->last_name}}</td>
                                                        <td>{{$useraccountVal->getAccount->getType->account_type}}</td>
                                                        <td>{{$useraccountVal->gender}}</td>
                                                        <td>{{date('M d, Y',strtotime($useraccountVal->getAccount->created_at))}}</td>
                                                        <td width="20%">
                                                            <div class="btn-group btn-group-solid">
                                                                <button type="button" class="btn btn-sm {{$useraccountVal->getAccount->status!=null  ? 'green-jungle' : 'red'}} userchangestatus" 
                                                                data-id         = "{{$useraccountVal->user_id}}"
                                                                data-username   = "{{$useraccountVal->getAccount->username}}"
                                                                data-status     = "{{$useraccountVal->getAccount->status!=null ? 'Activate' : 'Deactivate' }}"
                                                                data-toggle="modal" 
                                                                data-target="#changeStatus">{{$useraccountVal->getAccount->status!=null  ? 'Activate' : 'Deactivate'}}</button>
                                                                <button type="button" class="btn btn-sm green viewClick" data-toggle="modal" data-target="#viewProfile" style="margin-left:-5px"
                                                                data-id         = "{{$useraccountVal->user_id}}"
                                                                data-picture    = "{{$useraccountVal->getAccount->picture}}"
                                                                data-fname      = "{{$useraccountVal->first_name}}"
                                                                data-lname      = "{{$useraccountVal->last_name}}"
                                                                data-email      = "{{$useraccountVal->email}}"
                                                                data-birthdate  = "{{$useraccountVal->birthdate}}"
                                                                data-gender     = "{{$useraccountVal->gender}}"
                                                                data-position   = "{{$useraccountVal->getAccount->getType->account_type}}"
                                                                data-username   = "{{$useraccountVal->getAccount->username}}"
                                                                >View</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                    @include('include.viewProfile')


                                    <div class="modal fade" id="changeStatus" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body text-center"> 
                                                    <br/><br/>
                                                    <span class="fa fa-warning" style="font-size:100px;color:#F1C40F"></span>
                                                    <h1 id="msg">Are you sure you want to deactivate?</h1>
                                                </div>
                                                <div class="modal-footer">
                                                <form action="/admin/changestatus" method="post">
                                                    {!! csrf_field() !!}
                                                    <input type="hidden" name="id">
                                                    <input type="hidden" name="username">
                                                    <input type="hidden" name="status">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">No</button>
                                                    <button type="submit" class="btn green">Yes</button>
                                                </form>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
@include('include.splash')
@stop

@section('pagescript')
        <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/formvalidation.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="../assets/apps/custom/ajaxSave.js" type="text/javascript"></script>
        <script src="../js/toastr/toastr.min.js" type="text/javascript"></script>


        <script type="text/javascript">
            $(document).ready(function(){

                $('.userchangestatus').click(function(){
                    $('#msg').text("Are you sure you want to "+$(this).html()+"?");
                    $('input[name=id]').val($(this).attr('data-id'));
                    $('input[name=username]').val($(this).attr('data-username'));
                    $('input[name=status]').val($(this).attr('data-status'));
                });

                $('.viewClick').click(function(){
                    $('#profile-usertitle-name').text($(this).attr('data-fname')+" "+$(this).attr('data-lname'));
                    $('#userImg').attr('src', $(this).attr('data-picture')!="" ? '../assets/img/account/'+$(this).attr('data-picture') : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image');
                    $('#profile-usertitle-job').text($(this).attr('data-position'));
                    $('input[name=accountid]').val($(this).attr('data-id'));
                    $('input[name=fname]').val($(this).attr('data-fname'));
                    $('input[name=lname]').val($(this).attr('data-lname'));
                    $('input[name=email]').val($(this).attr('data-email'));
                    $('input[name=gender]').val($(this).attr('data-gender'));
                    $('input[name=birthdate]').val($(this).attr('data-birthdate'));
                    $('input[name=username]').val($(this).attr('data-username'));
                    $("option[id='"+$(this).attr('data-position')+"']").select($(this).attr('data-position'));
                });

                // $('.reload').click(function() {
                //         $.get(
                //             '../ajax/getaccount',
                //             function(response){
                //                 var parsedata = $.parseJSON(response);
                //                     console.log(parsedata);
                //                     $('tbody').html("");
                //                     for(var i=0; i<parsedata.length; i++) {
                //                         var status  = parsedata[i].get_account.deleted_at==null ? 'Deactivate' : 'Activate';
                //                         var btncolor    = parsedata[i].get_account.deleted_at==null ? 'red' : 'green-jungle';
                //                         $('tbody').append("<tr role='row'><th class='control' tabindex='0' style='display:none;'></th>"+
                //                                             "<td class='sorting_1'>"+parsedata[i].first_name+' '+parsedata[i].last_name +"</td>"+
                //                                             "<td>"+parsedata[i].get_account.get_type.account_type+"</td>"+
                //                                             "<td>"+parsedata[i].gender+"</td>"+
                //                                             "<td>"+parsedata[i].get_account.created_at+"</td>"+
                //                                             "<td width='20%'>"+
                //                                             "<div class='btn-group btn-group-solid'>"+
                //                                             "<button type='button' class='btn btn-sm "+btncolor+"' data-toggle='modal' data-target='#changeStatus'>"+status+"</button>"+
                //                                             "<button type='button' class='btn btn-sm green viewClick' data-toggle='modal' data-target='#viewProfile' style='margin-left:-5px'"+
                //                                             " data-id = '"+parsedata[i].get_account.user_id+"'"+
                //                                             " data-picture = '"+parsedata[i].get_account.picture+"'"+
                //                                             " data-fname='"+parsedata[i].first_name+"'"+
                //                                             " data-lname='"+parsedata[i].last_name+"'"+
                //                                             " data-email='"+parsedata[i].email+"'"+
                //                                             " data-birthdate='"+parsedata[i].birthdate+"'"+
                //                                             " data-gender='"+parsedata[i].gender+"'"+
                //                                             " data-position='"+parsedata[i].get_account.get_type.account_type+"'"+
                //                                             " data-username='"+parsedata[i].get_account.username+"'"+
                //                                             ">View</button>"+
                //                                             "</div>"+
                //                                             "</td></tr>");
                //                     }
                //                     console.log($('tbody').html);
                //             }
                //         );
                // });
            });
        </script>
@stop