@extends('layout.layoutmain')

@section('title')
<title>Add News | CentroBalita</title>
@stop

@section('headscript')
    <link href="../assets/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
@stop

@section('addnewsPage')
active open
@stop

@section('content')

                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Dashboard
                        <small>dashboard & statistics</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard">Home</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS 1-->
                    @include('include.response')
                    <div class="row">
                                        <div class="portlet light bordered padding-left-10">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-equalizer font-red-sunglo"></i>
                                                    <span class="caption-subject font-red-sunglo bold uppercase">Add News</span>
                                                    <span class="caption-helper">form actions without bg color</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="addarticle" method="post" id="form_requirements" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data">
                                                    {!! csrf_field() !!}
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Headline</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" name="headline" placeholder="Headline" required>
                                                                <span class="help-block"> A block of help text. </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Subheading</label>
                                                            <div class="col-md-9">
                                                                    <input type="text" class="form-control" name="subheading" placeholder="Subheading" required> 
                                                                    <span class="help-block"> A block of help text. </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Category</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                    <!-- <input type="text" class="form-control" name="category" id="ta_category" placeholder="Category" required>  -->
                                                                    <select class="form-control" name="category" required>
                                                                        <option></option>
                                                                            @foreach($category as $catVal)
                                                                            <option value="{{$catVal->category_id}}">{{$catVal->category}}</option>
                                                                            @endforeach
                                                                    </select> 
                                                                    <span class="input-group-addon" data-toggle="modal" data-target="#viewaddCategory">
                                                                    <i class="fa fa-plus font-red"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Province</label>
                                                            <div class="col-md-9">
                                                                <div class="input-group">
                                                                        <!-- <input type="text" class="form-control" name="province" id="ta_province" placeholder="Province" required>  -->
                                                                        <select class="form-control" name="province" required>
                                                                            <option></option>
                                                                            @foreach($province as $provVal)
                                                                            <option value="{{$provVal->province_id}}">{{$provVal->province}}</option>
                                                                            @endforeach
                                                                        </select> 
                                                                        <span class="input-group-addon" data-toggle="modal" data-target="#viewaddProvince">
                                                                            <i class="fa fa-plus font-red"></i>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-2">Image</label>
                                                            <div class="col-md-9">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="input-group input-large">
                                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                            <span class="fileinput-filename"> </span>
                                                                        </div>
                                                                        <span class="input-group-addon btn default btn-file">
                                                                            <span class="fileinput-new"> Select file </span>
                                                                            <span class="fileinput-exists"> Change </span>
                                                                            <input type="file" name="articleimg"> </span>
                                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">body</label>
                                                            <div class="col-md-10">
                                                                <textarea name="articlebody"  class="form-control" id="summernote_1" required> </textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group {{Auth::user()->account_type_id==3 ? hidden : ''}}">
                                                            <label class="col-md-2 control-label">Status</label>
                                                            <div class="col-md-9">
                                                                    <select class="form-control" name="status" required>
                                                                        <option value=""></option>
                                                                        <option value="Approved">Approved</option>
                                                                        <option value="Pending">Pending</option>
                                                                    </select>
                                                                    <span class="help-block"> A block of help text. </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-2 col-md-9">
                                                                <button type="submit" class="btn green">Submit</button>
                                                                <button type="button" class="btn blue">Preview</button>
                                                                <button type="button" id="test" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                    </div>
                    <div class="clearfix"></div>
                                    <div class="modal fade" id="viewaddCategory" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content" style="margin-top:25%;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Category</h4>
                                                </div>
                                                <div class="modal-body">
                                                <form action="#" method="post" class="form-horizontal addCategory">
                                                    {!! csrf_field() !!}
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Category</label>
                                                        <div class="col-md-10">
                                                                <input type="text" class="form-control" name="category" placeholder="Category" required> 
                                                        </div>
                                                    </div>
                                                </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green modalsaveCallback" data-form="addCategory" data-url="/ajax/addcategory" data-form-type="addCategory">Submit</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>

                                    <div class="modal fade" id="viewaddProvince" tabindex="-1" role="basic" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content" style="margin-top:25%;">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Add Province</h4>
                                                </div>
                                                <div class="modal-body">
                                                <form action="#" method="post" class="form-horizontal addProvince">
                                                    {!! csrf_field() !!}
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Province</label>
                                                        <div class="col-md-10">
                                                                <input type="text" class="form-control" name="province" placeholder="Province" required> 
                                                        </div>
                                                    </div>
                                                </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn green modalsaveCallback" data-form="addProvince" data-url="/ajax/addprovince" data-form-type="Add Province">Submit</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>

             
@stop

@section('pagescript')
        <script src="../assets/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-typeahead.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/formvalidation.js" type="text/javascript"></script>
        <script src="../assets/apps/custom/ajaxSave.js" type="text/javascript"></script>
        <script src="../js/toastr/toastr.min.js" type="text/javascript"></script>

        <script type="text/javascript">
            $('#test').click(function(){
                console.log($('textarea[name="description"]').html());
            });
            // $('#summernote_1').summernote({
            //     airMode:false
            // });
            $('.modalsaveCallback').click(function(){
                $('select[name=category]').html("");
                $('select[name=province]').html("");
                $('select[name=category]').append('<option></option>');
                $('select[name=province]').append('<option></option>');
                $.get('/ajax/category.json',
                        function(response){
                        var parser = $.parseJSON(response);
                            for(var i=0; i<parser.length; i++) {
                                $('select[name=category]').append('<option value='+parser[i].category_id+'>'+parser[i].category+'</option>');
                            }
                        });

                $.get('/ajax/province.json',
                        function(response){
                            var parser = $.parseJSON(response);
                            for(var i=0; i<parser.length; i++) {
                                $('select[name=province]').append('<option value='+parser[i].province_id+'>'+parser[i].province+'</option>');
                            }
                        });
            });

      // $.get('/ajax_classification',function(data){
      //   var parseholder = $.parseJSON(data);
      //   $('#ajax_subject').html('');
      //           $('#ajax_subject').append('<option value="">Select...<option');
      //           for(var i=0; i<parseholder.length; i++){
      //                    $('#ajax_subject').append('<option>'+parseholder[i].subject+'</option>'); 
      //                   console.log(parseholder[i].subject);
      //           }
      // });
        </script>
@stop