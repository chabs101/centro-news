
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Metronic | Invoice</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../assets/pages/css/invoice.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../assets/layouts/rjclayout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/layouts/rjclayout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="../assets/layouts/rjclayout2/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-boxed">

        <div class="row">
            <!-- BEGIN CONTENT -->
            <div class="col-md-12">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- END PAGE HEADER-->
                    <div class="invoice">
                        <div class="row invoice-logo">
                            <div class="col-xs-6 invoice-logo-space">
                                <img src="../assets/pages/media/invoice/walmart.png" class="img-responsive" alt="" /> </div>
                            <div class="col-xs-6">
                                <p> #5652256 / 28 Feb 2013
                                    <span class="muted"> Consectetuer adipiscing elit </span>
                                </p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-xs-4">
                                <h3>Client:</h3>
                                <ul class="list-unstyled">
                                    <li> John Doe </li>
                                    <li> Mr Nilson Otto </li>
                                    <li> FoodMaster Ltd </li>
                                    <li> Madrid </li>
                                    <li> Spain </li>
                                    <li> 1982 OOP </li>
                                </ul>
                            </div>
                            <div class="col-xs-4">
                                <h3>About:</h3>
                                <ul class="list-unstyled">
                                    <li> Drem psum dolor sit amet </li>
                                    <li> Laoreet dolore magna </li>
                                    <li> Consectetuer adipiscing elit </li>
                                    <li> Magna aliquam tincidunt erat volutpat </li>
                                    <li> Olor sit amet adipiscing eli </li>
                                    <li> Laoreet dolore magna </li>
                                </ul>
                            </div>
                            <div class="col-xs-4 invoice-payment">
                                <h3>Payment Details:</h3>
                                <ul class="list-unstyled">
                                    <li>
                                        <strong>V.A.T Reg #:</strong> 542554(DEMO)78 </li>
                                    <li>
                                        <strong>Account Name:</strong> FoodMaster Ltd </li>
                                    <li>
                                        <strong>SWIFT code:</strong> 45454DEMO545DEMO </li>
                                    <li>
                                        <strong>V.A.T Reg #:</strong> 542554(DEMO)78 </li>
                                    <li>
                                        <strong>Account Name:</strong> FoodMaster Ltd </li>
                                    <li>
                                        <strong>SWIFT code:</strong> 45454DEMO545DEMO </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th> # </th>
                                            <th> Item </th>
                                            <th class="hidden-xs"> Description </th>
                                            <th class="hidden-xs"> Quantity </th>
                                            <th class="hidden-xs"> Unit Cost </th>
                                            <th> Total </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> 1 </td>
                                            <td> Hardware </td>
                                            <td class="hidden-xs"> Server hardware purchase </td>
                                            <td class="hidden-xs"> 32 </td>
                                            <td class="hidden-xs"> $75 </td>
                                            <td> $2152 </td>
                                        </tr>
                                        <tr>
                                            <td> 2 </td>
                                            <td> Furniture </td>
                                            <td class="hidden-xs"> Office furniture purchase </td>
                                            <td class="hidden-xs"> 15 </td>
                                            <td class="hidden-xs"> $169 </td>
                                            <td> $4169 </td>
                                        </tr>
                                        <tr>
                                            <td> 3 </td>
                                            <td> Foods </td>
                                            <td class="hidden-xs"> Company Anual Dinner Catering </td>
                                            <td class="hidden-xs"> 69 </td>
                                            <td class="hidden-xs"> $49 </td>
                                            <td> $1260 </td>
                                        </tr>
                                        <tr>
                                            <td> 3 </td>
                                            <td> Software </td>
                                            <td class="hidden-xs"> Payment for Jan 2013 </td>
                                            <td class="hidden-xs"> 149 </td>
                                            <td class="hidden-xs"> $12 </td>
                                            <td> $866 </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="well">
                                    <address>
                                        <strong>Loop, Inc.</strong>
                                        <br/> 795 Park Ave, Suite 120
                                        <br/> San Francisco, CA 94107
                                        <br/>
                                        <abbr title="Phone">P:</abbr> (234) 145-1810 </address>
                                    <address>
                                        <strong>Full Name</strong>
                                        <br/>
                                        <a href="mailto:#"> first.last@email.com </a>
                                    </address>
                                </div>
                            </div>
                            <div class="col-xs-8 invoice-block">
                                <ul class="list-unstyled amounts">
                                    <li>
                                        <strong>Sub - Total amount:</strong> $9265 </li>
                                    <li>
                                        <strong>Discount:</strong> 12.9% </li>
                                    <li>
                                        <strong>VAT:</strong> ----- </li>
                                    <li>
                                        <strong>Grand Total:</strong> $12489 </li>
                                </ul>
                                <br/>
                                <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                                    <i class="fa fa-print"></i>
                                </a>
                                <a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your Invoice
                                    <i class="fa fa-check"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/rjclayout2/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/rjclayout2/scripts/demo.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>