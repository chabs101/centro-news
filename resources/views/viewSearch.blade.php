@extends('layout.layoutmain')

@section('headscript')
        <link href="../assets/pages/css/search.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
                    <div class="search-page search-content-2">
                        <form id="formsearch">
                            {!! csrf_field() !!} 
                            <div class="search-bar bordered">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search" placeholder="Search for...">
                                            <span class="input-group-btn">
                                                <button class="btn blue uppercase bold" id="searchbtn" type="button">Search</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="search-container bordered">
                                    <ul class="search-container" id="searchc">
                                        <li class="search-item clearfix">
                                            <div class="search-content text-left">
                                                <h2 class="search-title">
                                                    <a href="javascript:;">Metronic Search Results</a>
                                                </h2>
                                                <p class="search-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur pellentesque auctor. Morbi lobortis, leo in tristique scelerisque, mauris quam volutpat nunc </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
@stop

@section('pagescript')
     <script type="text/javascript">
        $('.search-form').hide();
        $('#searchbtn').click(function(){
            $.ajax({
                url:    '/ajax/testsearch',
                type:   'POST',
                data:   new FormData($("#formsearch")[0]),
                processData:    false,
                contentType:    false,
                success:function(response) {
                    $('#searchc').html("");
                    var parser = $.parseJSON(response)
                    console.log(parser);
                    for(var x=0; x<parser.length; x++) {
                        $('#searchc').append("<li class='search-item clearfix'><div class='search-content text-left'>"+
                            "<h2 class='search-title'><a href='javascript:;'>"+parser[x].headline+' '+parser[x].subheading+"</a></h2>"+
                            "<p class='search-desc'>"+parser[x].body+"</p></div></li>");
                    
                    }
                },
                error:function(response) {

                }
            });
        });

     </script>
@stop