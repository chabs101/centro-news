<form class="forget-form" action="#" method="post" novalidate="novalidate">
                <h3 class="font-green">Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-google" style="color:#000"></i>
                        <input type="email" class="form-control input-circle" placeholder="Email" name="email" style="color:#000;"> 
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn btn-default">Back</button>
                    <button type="submit" class="btn btn-danger uppercase pull-right">Submit</button>
                </div>
</form>