                                    <div class="modal fade" id="viewEditModal" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-full">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">Modal Title</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">

                                                        <div class="portlet light bordered padding-left-10">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="icon-equalizer font-red-sunglo"></i>
                                                                    <span class="caption-subject font-red-sunglo bold uppercase">Add News</span>
                                                                    <span class="caption-helper">form actions without bg color</span>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <!-- BEGIN FORM-->
                                                                <form action="/admin/updatearticle" method="post" id="form_requirements" class="form-horizontal form-bordered form-row-stripped">
                                                                    {!! csrf_field() !!}
                                                                    <input type="hidden" name="articleid"/>
                                                                    <div class="form-body">
                                                                            <div class="form-group">
                                                                                    <label class="control-label col-md-3">Headline</label>
                                                                                <div class="col-md-9">
                                                                                        <input type="text" class="form-control" name="headline" placeholder="Enter text">
                                                                                        <span class="help-block"> A block of help text. </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                    <label class="control-label col-md-3">Subheading</label>
                                                                                <div class="col-md-9">
                                                                                        <input type="text" class="form-control" name="subheading" placeholder="Enter text">
                                                                                        <span class="help-block"> A block of help text. </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Category</label>
                                                                                <div class="col-md-9">
                                                                                    <div class="input-group">
                                                                                        <!-- <input type="text" class="form-control" name="category" id="ta_category" placeholder="Category" required>  -->
                                                                                        <select class="form-control" name="category" required>
                                                                                            <option></option>
                                                                                                @foreach($category as $catVal)
                                                                                                <option value="{{$catVal->category_id}}">{{$catVal->category}}</option>
                                                                                                @endforeach
                                                                                        </select> 
                                                                                        <span class="input-group-addon" data-toggle="modal" data-target="#viewaddCategory">
                                                                                        <i class="fa fa-plus font-red"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Province</label>
                                                                                <div class="col-md-9">
                                                                                    <div class="input-group">
                                                                                            <!-- <input type="text" class="form-control" name="province" id="ta_province" placeholder="Province" required>  -->
                                                                                            <select class="form-control" name="province" required>
                                                                                                <option></option>
                                                                                                @foreach($province as $provVal)
                                                                                                <option value="{{$provVal->province_id}}">{{$provVal->province}}</option>
                                                                                                @endforeach
                                                                                            </select> 
                                                                                            <span class="input-group-addon" data-toggle="modal" data-target="#viewaddProvince">
                                                                                                <i class="fa fa-plus font-red"></i>
                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>                                                                            
                                                                        <div class="form-group">
                                                                                <label class="control-label col-md-3">Image</label>
                                                                            <div class="col-md-9">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="input-group input-large">
                                                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                                            <span class="fileinput-filename"> </span>
                                                                                        </div>
                                                                                        <span class="input-group-addon btn default btn-file">
                                                                                            <span class="fileinput-new"> Select file </span>
                                                                                            <span class="fileinput-exists"> Change </span>
                                                                                            <input type="file" name="articleimg"> </span>
                                                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                            <div class="form-group">
                                                                                <label class="control-label col-md-3">body</label>
                                                                                <div class="col-md-9">
                                                                                    <textarea name="articlebody"  class="form-control" id="summernote_1" required> </textarea>
                                                                                    <span id="test"></span>
                                                                                </div>
                                                                            </div>
                                                                    </div>
                                                                    <div class="form-actions right">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-2 col-md-9">
                                                                                <button type="submit" class="btn green">Submit</button>
                                                                                <button type="button" class="btn default" data-dismiss="modal">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                <!-- END FORM-->
                                                            </div>
                                                        </div>
                                    
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>