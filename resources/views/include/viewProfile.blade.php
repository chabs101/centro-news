                                    <div class="modal fade" id="viewProfile" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-full">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">View Profile</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <!-- BEGIN PROFILE SIDEBAR -->
                                                            <div class="profile-sidebar">
                                                                <!-- PORTLET MAIN -->
                                                                <div class="portlet light profile-sidebar-portlet ">
                                                                    <!-- SIDEBAR USERPIC -->
                                                                    <div style="margin-left:50px;">
                                                                        <img src="../assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt="" id="userImg" style="border-radius:50%;height:350px;width:80%;"> </div>
                                                                    <!-- END SIDEBAR USERPIC -->
                                                                    <!-- SIDEBAR USER TITLE -->
                                                                    <div class="profile-usertitle">
                                                                        <div class="profile-usertitle-name" id="profile-usertitle-name"> Marcus Doe </div>
                                                                        <div class="profile-usertitle-job" id="profile-usertitle-job"> Developer </div>
                                                                    </div>
                                                                    <!-- END SIDEBAR USER TITLE -->
                                                                </div>
                                                                <!-- END PORTLET MAIN -->
                                                            </div>
                                                            <!-- END BEGIN PROFILE SIDEBAR -->
                                                            <!-- BEGIN PROFILE CONTENT -->
                                                            <div class="profile-content">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="portlet light ">
                                                                            <div class="portlet-title tabbable-line">
                                                                                <div class="caption caption-md">
                                                                                    <i class="icon-globe theme-font hide"></i>
                                                                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                                                </div>
                                                                                <ul class="nav nav-tabs">
                                                                                    <li class="active">
                                                                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="#tab_1_3" data-toggle="tab">Change Username</a>
                                                                                    </li>

                                                                                    <li>
                                                                                        <a href="#tab_1_4" data-toggle="tab">Change Password</a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="portlet-body">
                                                                                <div class="tab-content">
                                                                                    <!-- PERSONAL INFO TAB -->
                                                                                    <div class="tab-pane active" id="tab_1_1">
                                                                                        <form role="form" action="#" class="updateUserInformation">
                                                                                           {!! csrf_field() !!}
                                                                                           <input type="hidden" name="accountid"> 
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">First Name</label>
                                                                                                <input type="text" placeholder="Raneil" name="fname" class="form-control" required />
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">Last Name</label>
                                                                                                <input type="text" placeholder="Chavez" name="lname" class="form-control" required />
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">Email</label>
                                                                                                <input type="email" placeholder="esmelet@gmail.com" name="email" class="form-control" required />
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">Gender</label>
                                                                                                <select name="gender" class="form-control" required>
                                                                                                    <option></option>
                                                                                                    <option>Male</option>
                                                                                                    <option>Female</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">Birthdate</label>
                                                                                                <input type="text" class="form-control date-picker" name="birthdate" placeholder="Birthdate" required> 
                                                                                            </div>
                                                                                        </form>
                                                                                            <div class="margin-top-10">
                                                                                                <a href="javascript:;" class="btn green modalsaveCallback" data-url="/admin/updateUserInformation" data-form="updateUserInformation" data-form-type="update User Information"> Save Changes </a>
                                                                                                <a href="javascript:;" class="btn default" data-dismiss="modal"> Cancel </a>
                                                                                            </div>
                                                                                    </div>
                                                                                    <!-- END PERSONAL INFO TAB -->
                                                                                    <div class="tab-pane" id="tab_1_2">
                                                                                        <form action="#" role="form" class="updateUserAvatar" enctype="multipart/form-data">
                                                                                           {!! csrf_field() !!}
                                                                                           <input type="hidden" name="accountid"> 
                                                                                            <div class="form-group">
                                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                                                    <div>
                                                                                                        <span class="btn default btn-file">
                                                                                                            <span class="fileinput-new"> Select image </span>
                                                                                                            <span class="fileinput-exists"> Change </span>
                                                                                                            <input type="file" name="userimg" required> </span>
                                                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="clearfix margin-top-10">
                                                                                                    <span class="label label-danger"> NOTE! </span>
                                                                                                    <span> Attached image thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                            <div class="margin-top-10">
                                                                                                <a href="javascript:;" class="btn green modalsaveCallback" data-url="/admin/updateUserAvatar" data-form="updateUserAvatar" data-form-type="Update User Avatar"> Change Avatar </a>
                                                                                                <a href="javascript:;" class="btn default" data-dismiss="modal"> Cancel </a>
                                                                                            </div>
                                                                                    </div>

                                                                                    <div class="tab-pane" id="tab_1_3">
                                                                                        <form action="#" class="updateUsername">
                                                                                           {!! csrf_field() !!}
                                                                                           <input type="hidden" name="accountid"> 
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">New Username</label>
                                                                                                <input type="text" name="username" class="form-control" required/> </div>
                                                                                        </form>
                                                                                            <div class="margin-top-10">
                                                                                                <a href="javascript:;" data-url="/admin/updateUsername" data-form="updateUsername" data-form-type="Update Username" class="btn green modalsaveCallback"> Change Username </a>
                                                                                                <a href="javascript:;" class="btn default" data-dismiss="modal"> Cancel </a>
                                                                                            </div>
                                                                                    </div>
                                                                                    <!-- CHANGE PASSWORD TAB -->
                                                                                    <div class="tab-pane" id="tab_1_4">
                                                                                        <form action="#" class="updateUserPassword">
                                                                                           {!! csrf_field() !!}
                                                                                           <input type="hidden" name="accountid"> 
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">Admin Password</label>
                                                                                                <input type="password" class="form-control" name="curpassword" required/> 
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">New Password</label>
                                                                                                <input type="password" class="form-control" name="password" required/> 
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="control-label">confirm Password</label>
                                                                                                <input type="password" class="form-control" name="newpassword" required/> 
                                                                                            </div>
                                                                                        </form>
                                                                                            <div class="margin-top-10">
                                                                                                <a href="javascript:;" data-url="/admin/updateUserPassword" data-form="updateUserPassword" data-form-type="Update User Password" class="btn green modalsaveCallback"> Change Password </a>
                                                                                                <a href="javascript:;" class="btn default" data-dismiss="modal"> Cancel </a>
                                                                                            </div>
                                                                                    </div>
                                                                                    <!-- END CHANGE PASSWORD TAB -->

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END PROFILE CONTENT -->
                                                        </div>
                    
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>