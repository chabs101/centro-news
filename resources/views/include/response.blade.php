        @if (Session::has('fail'))
            <div class="alert alert-danger {{Session::has('fail') ? 'alert-important' : ''}}">
                @if(Session::has('fail'))
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                @endif
            {{Session::get('failmsg') }}
            </div>
        @endif

        @if (Session::has('success'))
            <div class="alert alert-success {{Session::has('success') ? 'alert-important' : ''}}">
                @if(Session::has('success'))
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                @endif
            {{Session::get('successmsg') }}
            </div>
        @endif