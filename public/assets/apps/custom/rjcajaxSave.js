
  $('body').append('<div class="modal draggable-modal " id="modalsuccess" tabindex="-1" role="basic" aria-hidden="true"><div class="modal-dialog "><div class="modal-content bg-blue bg-font-blue"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h2><i class="icon-check"></i>&nbsp; <span class="messagesuccess"> sample</span></h2> </div></div></div>');
  $('body').append('<div class="modal draggable-modal " id="modalfailed" tabindex="-1" role="basic" aria-hidden="true"><div class="modal-dialog "><div class="modal-content bg-red bg-font-red"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h2><i class="icon-close"></i>&nbsp; <span class="messagefailed">sample</span></h2> </div></div></div>');
  $('body').append('<div class="modal draggable-modal " id="modalwarning" tabindex="-1" role="basic" aria-hidden="true"><div class="modal-dialog "><div class="modal-content bg-yellow-casablanca bg-font-yellow-casablanca"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button><h2><i class="fa fa-warning"></i>&nbsp; <span class="messagewarning">sample</span></h2> </div></div></div>');
   // $('#modalwarning').addClass('animated shake');
   //              $('#modalwarning').modal('show');
   //              $('.messagefailed').text('bobo');

    $('.modalsaveCallback').click(function(){
        var url = $(this).attr('data-url');
        var form_type = $(this).attr('data-form-type');
        var form_id = $(this).attr('data-form');
        var form = $("."+form_id)[0];
        var formdata = new FormData(form);
        console.log(form);
        // console.log(data);

        formValidate = $(form);

        formValidate.validate({
            ignoreTitle: true,
            debug:true,

            errorPlacement: function(error, element){

              error.insertAfter(element);
            }

        });

        if(!formValidate.valid()){
                $('#modalwarning').addClass('animated bounceIn');
                $('#modalwarning').modal('show');
                message = 'Some input fields is highly required. Please write something to resume.';
                $('.messagewarning').text(' '+message);
        }
        

        $.ajax( {
            url: url,
            type:'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success:function(data){
                if(data[0]==true){
                $('#modalsuccess').addClass('animated fadeIn');
                $('#modalsuccess').modal('show');
                $('.messagesuccess').text(' '+data[1]);
                form.reset();
                updateview();
                }

                if(data[0]==false){
                $('#modalfailed').addClass('animated shake');
                $('#modalfailed').modal('show');
                $('.messagefailed').text(' '+data[1]);
                }
            },
            error:function(error){
                  // form.reset();
                  fail('Sorry, '+form_type+' failed to submit.')
            }
        });

    });

 // ajax update subject


    function updatesubject(){
      $.get('/ajax_classification',function(data){
        var parseholder = $.parseJSON(data);
        $('#ajax_subject').html('');

                for(var i=0; i<parseholder.length; i++){
                         $('#ajax_subject').append('<option>'+parseholder[i].subject+'</option>'); 
                        console.log(parseholder[i].subject);
                }
      });
    }

    function success(message){

        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": true,
          "positionClass": "toast-top-full-width",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "3000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

        toastr["success"](message)

    }


    function fail(message){
        
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": true,
          "progressBar": true,
          "positionClass": "toast-bottom-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

        toastr["warning"](message)

    }