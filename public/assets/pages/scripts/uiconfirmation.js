var UIConfirmations = function () {

    var handleSample = function () {
        
        $('.Approved_confirmation').on('confirmed.bs.confirmation', function () {
            $('.splash-body').show();
            $('#'+$(this).attr('data-articlepending-id')).remove().appendTo('#approvedarticlecontainer');
            $('#'+$(this).attr('data-btn-id')).remove();
            $('.pending-article-'+$(this).attr('data-article-id')).removeClass().addClass('mt-comment-status font-green').text('APPROVED')
            $.get('/ajax/confirmarticle',
                    {approved : 'Approved',
                    articleid : $(this).attr('data-article-id'),
                    headline  : $(this).attr('data-headline')
                    },
                    function(response){
                        var parsedata = $.parseJSON(response);
                            console.log(parsedata[0]);
                        if(parsedata[0]==true) {
                            $('.splash-body').fadeOut('slow');
                            $('#modalsuccess').addClass('animated fadeIn');
                            $('#modalsuccess').modal('show');
                            $('.messagesuccess').text(' '+parsedata[1]);
                        }
                    }
                );
        });

        $('.Approved_confirmation').on('canceled.bs.confirmation', function () {
            alert('You canceled action #1');
        });   

        $('.Reject_confirmation').on('confirmed.bs.confirmation', function () {
            $('#'+$(this).attr('data-articlepending-id')).remove().appendTo('#rejectedarticlecontainer');
            $('#'+$(this).attr('data-btn-id')).remove();
            $('.pending-article-'+$(this).attr('data-article-id')).removeClass().addClass('mt-comment-status font-red').text('REJECTED')
            $.get('/ajax/confirmarticle',
                    {rejected : 'Rejected',
                    articleid : $(this).attr('data-article-id'),
                    headline  : $(this).attr('data-headline')
                    },
                    function(response){
                        var parsedata = $.parseJSON(response);
                            console.log(parsedata[0]);
                        if(parsedata[0]==true) {
                            $('#modalfailed').addClass('animated shake');
                            $('#modalfailed').modal('show');
                            $('.messagefailed').text(' '+parsedata[1]);
                        }
                    }
                );
        });

        $('.Reject_confirmation').on('canceled.bs.confirmation', function () {
            alert('You canceled action #2');
        });
    }


    return {
        //main function to initiate the module
        init: function () {

           handleSample();

        }

    };

}();

jQuery(document).ready(function() {    
   UIConfirmations.init();
});